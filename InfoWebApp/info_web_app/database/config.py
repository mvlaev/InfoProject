from sqlalchemy     import create_engine
from sqlalchemy.orm import declarative_base
from sqlalchemy.orm import sessionmaker

from .. import settings

engine       = create_engine(settings.SQLALCHEMY_DATABASE_URL, echo=True, future=True)
Base         = declarative_base()
LocalSession = sessionmaker(autocommit=False, autoflush=False, bind=engine)
