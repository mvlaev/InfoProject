from enum import Enum
from datetime import date

from sqlalchemy import select, update
from sqlalchemy.orm import Session

from . import models
from ..schema import data_schema as schema



class ModelsEnum(Enum):
	admins   = models.Admin
	clients  = models.ClientProfile
	readings = models.ClientReadings
	electro  = models.Electricity
	warmup   = models.Warmup
	water    = models.Water
	trash    = models.Trash
	common_warmup = models.CommonWarmup

def create_something(what: str, something: list[schema.AdminSchema]|
										   list[schema.ClientProfileSchema]|
										   list[schema.UploadClientReadingsSchema]|
										   list[schema.ElectroSchema]|
										   list[schema.WarmupSchema]|
										   list[schema.WaterSchema]|
										   list[schema.TrashSchema]|
										   list[schema.CommonWarmupSchema],
					 db_session: Session,
					 msg: dict):

	model = ModelsEnum[what].value

	for schema in something:
		item = model(**schema.dict())
		db_session.add(item)
		msg['create'].append(f'{what} created')
	db_session.commit()
	return msg

def get_clients(contract_ids: list[int], db_session: Session):
	stmt = select(models.ClientProfile).where(models.ClientProfile.contract_id.in_(contract_ids))
	clients = db_session.execute(stmt).scalars().all()
	#if not clients:
		#return None
	#return schema.ClientProfileSchema.from_orm(client)
	return clients

def get_admins_by_username(usernames: list[str], db_session: Session):
	stmt = select(models.Admin).where(models.Admin.username.in_(usernames))
	admins = db_session.execute(stmt).scalars().all()
	return admins

def get_all_things(what: str, db_session: Session):
	stmt = select(ModelsEnum[what].value)
	return db_session.execute(stmt).scalars().all()

def get_client_readings_by_contract_id(contract_id: int, db_session: Session):
	stmt = select(models.ClientReadings).where(models.ClientReadings.contract_id == contract_id)
	return db_session.execute(stmt).scalars().all()

def get_client_readings_by_url(url: str, db_session: Session):
	stmt = select(models.ClientReadings).where(models.ClientReadings.url_suffix == url)
	readings = db_session.execute(stmt).scalar()

	if readings:
		response_schema = schema.ResponseSchema.from_orm(readings)

		#TODO: LOOK OUT FOR EMPTY INFOs !!!!!
		###SET THE TOTAL VARIABLES OF THE ResponseSchema
		#Electricity
		total_el = sum([data.consumption for data in response_schema.electricity_info])
		response_schema.total_el = total_el

		total_el_price = sum([data.price_end for data in response_schema.electricity_info])
		response_schema.total_el_price = total_el_price
		##############

		#Warmup
		total_warmup = sum([data.energy_plus_maintenance for data in response_schema.warmup_info])
		response_schema.total_warmup = total_warmup

		total_warmup_price = sum([data.price_end for data in response_schema.warmup_info])
		response_schema.total_warmup_price = total_warmup_price
		################
		###
		return response_schema
		#return readings
	return None

#def get_client_readings_by_date(date: date, db_session: Session):
	#stmt = select(models.ClientReadings).where(models.ClientReadings.date == date)
	#return db_session.execute(stmt).scalars().all()

#def update_something(what: str, where_attr, where_condition, update_values: dict, db_session: Session):
	#stmt = update(ModelsEnum[what].value).where(where_attr == where_condition).values(**update_values)
	#db_session.execute(stmt)
	#db_session.commit()

def update_something(things: list[tuple], db_session: Session, msg: dict):
	'''Generic Update Function.

	TODO: Add description.
	'''

	for thing in things:
		something, update_values = thing
		for attr, value in update_values.dict().items():
			if hasattr(something, attr):
				setattr(something, attr, value)
			else:
				raise Exception('database error')
		#db_session.add(something)
		msg['update'].append(f'updated {something}')
	db_session.commit()
	return msg

def delete_something(things: list, db_session: Session, msg: dict):
	for thing in things:
		db_session.delete(thing)
		msg['delete'].append(f'deleted {thing}')
	db_session.commit()
	return msg

def delete_old_something(what:str, before_date, db_session: Session, msg: dict):
	model = ModelsEnum[what].value

	stmt = select(model).where(
								(model.date < before_date)|
								(model.date == None)
								)
	msg = delete_something(things=db_session.execute(stmt).scalars().all(), db_session=db_session, msg=msg)

	return msg

def delete_something_on_date(what: str, date: date, db_session: Session, msg: dict):
	model = ModelsEnum[what].value
	stmt = select(model).where(
								(model.date == date)|
								(model.date == None)
								)
	msg = delete_something(things=db_session.execute(stmt).scalars().all(), db_session=db_session, msg=msg)
	return msg


