from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Date, Float
from sqlalchemy.orm import relationship

from .config import Base

class Admin(Base):
	__tablename__ = 'administrators'

	id_ = Column(Integer, primary_key=True, autoincrement=True)
	username = Column(String, unique=True, nullable=False)
	hashed_pass = Column(String, nullable=False)
	role = Column(String, nullable=False)
	email = Column(String, nullable=True)

class ClientProfile(Base):
	__tablename__ = 'client_profiles'

	#id_ = Column(Integer, primary_key=True)
	contract_id = Column(Integer, primary_key=True)

	full_name = Column(String, nullable=False)
	email = Column(String, nullable=False, unique=False)
	wants_report = Column(Boolean, nullable=False)

	readings = relationship('ClientReadings', back_populates='client_info', cascade='all, delete-orphan')

class ClientReadings(Base):
	__tablename__ = 'client_readings'

	#id_         = Column(Integer, primary_key=True)
	contract_id = Column(Integer, ForeignKey('client_profiles.contract_id'))

	date        = Column(Date, nullable=False)
	url_suffix  = Column(String, nullable=False, unique=True, primary_key=True)

	client_info      = relationship('ClientProfile', back_populates='readings')
	electricity_info = relationship('Electricity',
									primaryjoin='and_(ClientReadings.contract_id==Electricity.contract_id, ClientReadings.date==Electricity.date)',
									uselist=True,
									cascade='all, delete-orphan')

	trash_info       = relationship('Trash',
									primaryjoin='and_(ClientReadings.contract_id==Trash.contract_id, ClientReadings.date==Trash.date)',
									#uselist=True,
									cascade='all, delete-orphan')

	warmup_info      = relationship('Warmup',
									primaryjoin='and_(ClientReadings.contract_id==Warmup.contract_id, ClientReadings.date==Warmup.date)',
									#uselist=False,
									cascade='all, delete-orphan')

	water_info       = relationship('Water',
									primaryjoin='and_(ClientReadings.contract_id==Water.contract_id, ClientReadings.date==Water.date)',
									#uselist=False,
									cascade='all, delete-orphan'
									)

	#common_electricity_info = None
	#common_trash_info       = None
	#common_water_info       = None
	common_warmup_info      = relationship('CommonWarmup',
									primaryjoin='ClientReadings.date==CommonWarmup.date',
									uselist=True,
									#cascade='all, delete-orphan'
									)

class Electricity(Base):
	__tablename__ = 'electricity'

	#reading_id  = Column(Integer, primary_key=True, autoincrement=True)
	contract_id = Column(Integer, ForeignKey('client_readings.contract_id'), nullable=False, primary_key=True)
	date        = Column(Date, ForeignKey('client_readings.date'), nullable=False, primary_key=True)

	meter_name          = Column(String, nullable=False, primary_key=True)
	transformation_coef = Column(Integer, nullable=False)
	participation_coef  = Column(Float, nullable=False)
	reading_old         = Column(Float, nullable=False)
	reading_new         = Column(Float, nullable=False)
	reading_diff        = Column(Float, nullable=False)
	consumption         = Column(Float, nullable=False)
	price_single        = Column(Float, nullable=False)
	price_end           = Column(Float, nullable=False)

class Trash(Base):
	__tablename__ = 'trash'

	contract_id = Column(Integer, ForeignKey('client_readings.contract_id'), nullable=False, primary_key=True)
	date        = Column(Integer, ForeignKey('client_readings.date'), nullable=False, primary_key=True)



class Warmup(Base):
	__tablename__ = 'warmup'

	contract_id    = Column(Integer, ForeignKey('client_readings.contract_id'), nullable=False,)
	date           = Column(Date, ForeignKey('client_readings.date'), nullable=False, primary_key=True,)

	unit                    = Column(String, nullable=False, primary_key=True)

	building                = Column(String, nullable=False, primary_key=True,)
	volume                  = Column(Float, nullable=False)
	coef                    = Column(Float, nullable=False)
	reduced_volume          = Column(Float, nullable=False)
	power                   = Column(Float, nullable=True)
	energy                  = Column(Float, nullable=False)
	energy_plus_maintenance = Column(Float, nullable=False)
	price_end               = Column(Float, nullable=False)

class Water(Base):
	__tablename__ = 'water'

	contract_id = Column(Integer, ForeignKey('client_readings.contract_id'), nullable=False, primary_key=True)
	date        = Column(Integer, ForeignKey('client_readings.date'), nullable=False, primary_key=True)

#class CommonElectricity(Base): pass
#class CommonTrash(Base):
	#pass

class CommonWarmup(Base):
	__tablename__ = 'common_warmup'

	entry                   = Column(Integer, primary_key=True)
	date                  = Column(Date, ForeignKey('client_readings.date'), nullable=True)
	building              = Column(String, nullable=False)
	total_energy          = Column(Float, nullable=False)
	total_reduced_volume  = Column(Float, nullable=True,)
	energy_reduced_volume = Column(Float, nullable=True,)

	total_power      = Column(Float, nullable=True,)
	energy_per_power = Column(Float, nullable=True,)

	maintenance_percent = Column(Float, nullable=False)

	single_price = Column(Float, nullable=False)
	total_price  = Column(Float, nullable=False)

#class CommonWater(Base): pass
