from fastapi import FastAPI, Depends, HTTPException
from starlette.responses import HTMLResponse
from starlette.requests import Request
from starlette.templating import Jinja2Templates

#from . import data_administration
from ..frontend import admin_ops
#from ..schema import data_schema as schema
from ..database import crud_ops
from ..dependencies import deps

###Test run
from ..database import models
from ..database.config import engine
models.Base.metadata.create_all(bind=engine)
###

app = FastAPI()

# TODO: ISN'T IT BETTER WITH THE FOLLOWING??'
###TEST TEMPLATES
#from pathlib import Path
#templates_path = Path(__file__, '../../../templates').resolve()
#if not templates_path.exists(): raise Exception(f'templates not found at {templates_path.resolve()}')

templates = Jinja2Templates(directory='InfoWebApp/templates')

########

@app.get('/')
def root():
	return {'message': 'WELCOME!'}

@app.post('/{url}', response_class=HTMLResponse)
def get_spravka(request: Request,url: str, db_session=Depends(deps.get_session)):
	data = crud_ops.get_client_readings_by_url(url, db_session)

	if not data:
		raise HTTPException(status_code=404, detail=f'No report found for url: {url}')
	return templates.TemplateResponse('report.jinja2.html', {'request':request, 'data':data})

app.include_router(admin_ops.admin_router)

### THIS IS DEPRECATED
#@app.post('/token')
#def get_token(credentials: None):
	##TODO: GENERATE TOKEN
	#token = 'asd'
	#return token

### DEPRECATED
#app.include_router(data_administration.data_router)


