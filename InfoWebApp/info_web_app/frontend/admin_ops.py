'''Administration operations' views.

This module provides Access points for the
administrators.
'''

#import tempfile
#from pathlib import Path

from fastapi import APIRouter, Depends#, Form, UploadFile, File, HTTPException

from starlette            import status
from starlette.responses  import HTMLResponse, RedirectResponse
from starlette.requests   import Request
from starlette.templating import Jinja2Templates

from ..auth.auth_ops           import UserAuth, JWTOps
from ..database                import crud_ops
from ..dependencies            import deps, auth_classes
#from ..logic                   import upload_data_logic
#from ..logic.info_data_manager import data_prepare
from ..schema                  import data_schema as schema

templates = Jinja2Templates(directory='InfoWebApp/templates')

admin_router = APIRouter(
	prefix='/admin',
	)


#####################################################################


###ACTION RESULT
@admin_router.post('/result/{info}')
def action_result(info: str, request: Request, permission = Depends(auth_classes.BaseTokenCheck)):
	if not permission.permission_granted:
		decrypted_info = JWTOps.validate_json_token(permission.encrypted_result)
	else:
		decrypted_info = JWTOps.validate_json_token(info)

	result = schema.ActionResult(**decrypted_info)
	return templates.TemplateResponse('action_result.jinja2.html', {'request':request, 'csrf_token':permission.personal_jwt_container.token, 'result':result})
########################################################


######################################################################


###LOGIN ###############################################
@admin_router.get('/', response_class=HTMLResponse)
def login_form(request: Request):
	# TODO: GENERATE RANDOM ANONYMOUS USER
	anonymous = schema.AdminInfoSchema(username='anonymous')

	csrf_json_token = JWTOps.generate_json_token(data=anonymous.dict())

	return templates.TemplateResponse('login_form.jinja2.html', {'request':request, 'csrf_token': csrf_json_token, 'errors':None})


@admin_router.post('/login', response_class=HTMLResponse)
def login_action(request: Request, permission = Depends(auth_classes.PermissionToLogin)):
	# 1. CHECK THE csrf_token
	# 2. AUTHENTICATE username AND password
	# 3. GENERATE NEW csrf_token
	# 4. RETURN

	return templates.TemplateResponse('action_result.jinja2.html',
									  {
									   'request':request,
									   'csrf_token':permission.personal_jwt_container.token,
									   'result':permission.result
									   },
									  )
	#return RedirectResponse(f'/admin/result/{permission.encrypted_result}', status_code=status.HTTP_308_PERMANENT_REDIRECT)
########################

#############################################################

###DASHBOARD
@admin_router.post('/dashboard', response_class=HTMLResponse)
def dashboard(request: Request, permission = Depends(auth_classes.PermissionDashboard)):
	if not permission.permission_granted:
		return RedirectResponse(f'/admin/result/{permission.encrypted_result}', status_code=status.HTTP_308_PERMANENT_REDIRECT)
		#return templates.TemplateResponse('action_result.jinja2.html', {'request':request, 'csrf_token':personal_jwt_container.token, 'result':result})
	return templates.TemplateResponse('dashboard.jinja2.html', {'request': request, 'csrf_token': permission.personal_jwt_container.token, 'user':permission.personal_jwt_container.admin_info})
################


##############################################################################


###DATA UPLOAD
@admin_router.post('/fileupload', response_class=HTMLResponse)
def file_upload_form(request: Request, permission = Depends(auth_classes.PermissionToUploadData)):
	if not permission.permission_granted:
		return RedirectResponse(f'/admin/result/{permission.encrypted_result}', status_code=status.HTTP_308_PERMANENT_REDIRECT)
	return templates.TemplateResponse('upload_data.jinja2.html', {'request': request, 'csrf_token':permission.personal_jwt_container.token})

@admin_router.post('/data-upload')#, response_class=HTMLResponse)
def file_upload_action( request: Request, upload = Depends(deps.UploadManagement)):
	return RedirectResponse(f'/admin/result/{upload.encrypted_result}', status_code=status.HTTP_308_PERMANENT_REDIRECT)
###########################################


###############################################################################


###USER MANAGEMENT##########################
@admin_router.post('/create', response_class=HTMLResponse)
def user_create_form(request: Request,
					 permission = Depends(auth_classes.PermissionToCreateAdmin),
					 ):

	if not permission.permission_granted:
		return RedirectResponse(f'/admin/result/{permission.encrypted_result}', status_code=status.HTTP_308_PERMANENT_REDIRECT)

	#2. DEFINE AVAILABLE ROLES THAT THE CURRENT USER CAN ASSIGN
	available_roles = UserAuth.can_manage(permission.personal_jwt_container.admin_info.role)

	return templates.TemplateResponse('create_form.jinja2.html', {'request':request, 'csrf_token': permission.personal_jwt_container.token, 'roles':available_roles})


@admin_router.post('/create/user')
def user_create_action(
				request: Request,
				action = Depends(deps.CreateAdmin),
				):
	return RedirectResponse(f'/admin/result/{action.encrypted_result}', status_code=status.HTTP_308_PERMANENT_REDIRECT)


@admin_router.post('/all', response_class=HTMLResponse)
def list_all_admins(request: Request,
					admins = Depends(deps.ListAdmins),
				):
	#1. VERIFY TOKEN
	if not admins.permission.permission_granted:
		return RedirectResponse(f'/admin/result/{admins.encrypted_result}', status_code=status.HTTP_308_PERMANENT_REDIRECT)
	return templates.TemplateResponse('admin_list.jinja2.html', {'request':request, 'csrf_token':admins.permission.personal_jwt_container.token, 'admins':admins.admins})
#######################################################


############################################################################


### UPDATE OPERATIONS ########################
@admin_router.post('/all/{username}')
def user_update_form(request: Request,
					 username: str,
					 permission = Depends(auth_classes.PermissionToUpdate),
					 db_session = Depends(deps.get_session)):

	if not permission.permission_granted:
		return RedirectResponse(f'/admin/result/{permission.encrypted_result}', status_code=status.HTTP_308_PERMANENT_REDIRECT)

	#3. DEFINE THE ROLES THAT THE CURRENT ADMIN CAN MANAGE.
	roles = UserAuth.can_manage(permission.personal_jwt_container.admin_info.role)

	#4. GET THE USER THAT IS TO BE UPDATED
	updated_user = crud_ops.get_admins_by_username(usernames=[username], db_session=db_session)[0]

	return templates.TemplateResponse('update_form.jinja2.html', {'request':request, 'csrf_token':permission.personal_jwt_container.token, 'details_for_user':username, 'user_role':updated_user.role, 'roles':roles})


@admin_router.post('/all/{username}/update', response_class=HTMLResponse)
def user_update_action(request: Request,
				update_admin = Depends(deps.UpdateAdmin)
				):
	return RedirectResponse(f'/admin/result/{update_admin.encrypted_result}', status_code=status.HTTP_308_PERMANENT_REDIRECT)
#######################################


#########################################################


### DELETE OPERATIONS #################
@admin_router.post('/all/{username}/delete')
def user_delete(request: Request,
				operation = Depends(deps.DeleteAdmin)
				):
	return RedirectResponse(f'/admin/result/{operation.encrypted_result}', status_code=status.HTTP_308_PERMANENT_REDIRECT)
#######################################


#########################################################


###REPORTS
@admin_router.post('/reports/client', response_class=HTMLResponse)
def client_report_form(request: Request, operation = Depends(deps.GetClients)):
	#1. VERIFY TOKEN
	if not operation.permission.permission_granted:
		return RedirectResponse(f'/admin/result/{operation.encrypted_result}', status_code=status.HTTP_308_PERMANENT_REDIRECT)
	return templates.TemplateResponse('client_reports_select_client.jinja2.html', {'request':request, 'csrf_token':operation.permission.personal_jwt_container.token, 'users':operation.clients})

@admin_router.post('/reports/client/readings', response_class=HTMLResponse)
def client_report_action(request: Request,
						 operation = Depends(deps.GetClientReports),
						):
	#1. VERIFY TOKEN
	if not operation.permission.permission_granted:
		return RedirectResponse(f'/admin/result/{operation.encrypted_result}', status_code=status.HTTP_308_PERMANENT_REDIRECT)
	return templates.TemplateResponse('client_reports_select_reading.jinja2.html', {'request':request, 'readings':operation.readings})
