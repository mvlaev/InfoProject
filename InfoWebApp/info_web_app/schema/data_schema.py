from datetime import date
from enum import Enum

from pydantic import BaseModel

class BasicInfo(BaseModel):
	contract_id: int

class BasicUploadInfo(BasicInfo):
	date: date

class ClientProfileSchema(BasicInfo):
	full_name: str
	email: str
	wants_report: bool = True

	class Config:
		orm_mode = True


class UploadClientReadingsSchema(BasicUploadInfo):
	url_suffix: str

class ElectroSchema(BasicUploadInfo):
	meter_name: str
	transformation_coef: int
	participation_coef: float
	reading_old: float
	reading_new: float
	reading_diff: float
	consumption: float
	price_single: float
	price_end: float

	class Config:
		orm_mode = True


class WaterSchema(BasicUploadInfo):pass

class WarmupSchema(BasicUploadInfo):
	unit                    : str
	building                : str
	volume                  : float
	coef                    : float
	reduced_volume          : float
	power                   : float|None = None
	energy                  : float
	energy_plus_maintenance : float
	price_end               : float

	class Config:
		orm_mode = True

class TrashSchema(BasicUploadInfo): pass

class CommonsSchema(BaseModel):
	#entry: int
	date: date
	building: str

class CommonWarmupSchema(CommonsSchema):
	total_energy          : float
	total_reduced_volume  : float | None = None
	energy_reduced_volume : float | None = None

	total_power      : float | None = None
	energy_per_power : float | None = None

	maintenance_percent: float

	single_price : float
	total_price  : float

	class Config:
		orm_mode = True

class UploadSchema(BaseModel):
	clients:       list[ClientProfileSchema]       |None = None
	readings:      list[UploadClientReadingsSchema]|None = None
	electro:       list[ElectroSchema]             |None = None
	warmup:        list[WarmupSchema]              |None = None
	water:         list[WaterSchema]               |None = None
	trash:         list[TrashSchema]               |None = None

	common_warmup: list[CommonWarmupSchema]        |None = None
	#common_water:
	#common_trash:
	#common_electricity:

	class Config:
		orm_mode = True

class ResponseSchema(BaseModel):
	date: date
	client_info: ClientProfileSchema

	electricity_info: list[ElectroSchema]
	total_el: float|None = None
	total_el_price : float|None = None

	warmup_info: list[WarmupSchema]
	total_warmup: float | None = None
	total_warmup_price: float|None = None

	common_warmup_info: list[CommonWarmupSchema]

	class Config:
		orm_mode = True


###ADMIN  SCHEMAS
class AdminRoles(str, Enum):
	root  = 'root'
	admin = 'admin'
	op    = 'op'
	none  = 'no role'

class SubRoles(Enum):
	root      = ['root', 'admin', 'op']
	admin     = ['admin', 'op']
	op        = []
	all_roles = ['root', 'admin', 'op']

class AdminInfoSchema(BaseModel):
	username: str
	role: AdminRoles = AdminRoles.none
	email: str|None = None

	class Config:
		orm_mode = True

class AdminSchema(AdminInfoSchema):
	hashed_pass: str

	class Config:
		orm_mode = True

class TokenContainer(BaseModel):
	token: str = None
	data: dict = None
	error: str = None
	admin_info: AdminInfoSchema|None = None

class ActionResult(BaseModel):
	operation: str
	msg: str
	next_url: str
