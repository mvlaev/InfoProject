'''Settings for the auth package.

'''
from secrets import token_hex

from passlib.context import CryptContext

from ..schema import data_schema as schema

### JWT SETTINGS
KEY        = token_hex(32)
JWT_ENCODE_ALG     = 'HS256'
KEY_MANAGEMENT_ALG = 'dir'
ENCRYPTION_ALG     = 'A256CBC-HS512'

TOKEN_EXPIRES_MIN     = 15
##############

###passlib SETTINGS
PSWD_CONTEXT = CryptContext(['sha512_crypt', 'pbkdf2_sha512'])
################

###PERMISSIONS BY ROLE
SUBROLES = schema.SubRoles
