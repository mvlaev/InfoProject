'''Authentication and authorization operations.

'''
from datetime import datetime, timedelta
import codecs

from sqlalchemy.orm import Session
from jose import jwe, jwt

from . import settings
from ..schema import data_schema as schema
from ..database import crud_ops




class JWTOps:
	key                = settings.KEY
	jwt_encode_alg     = settings.JWT_ENCODE_ALG
	key_management_alg = settings.KEY_MANAGEMENT_ALG
	encryption_alg     = settings.ENCRYPTION_ALG
	expiration_delta   = settings.TOKEN_EXPIRES_MIN

	@staticmethod
	def generate_json_token(data: dict|None = None):

		if not data:
			data = {}

		###DATA DICT:
		##ADD EXPIRATION:
		data.update(exp=(datetime.utcnow() + timedelta(minutes=JWTOps.expiration_delta)))

		token = jwt.encode(data, JWTOps.key, algorithm=JWTOps.jwt_encode_alg)
		encrypted = jwe.encrypt(token, JWTOps.key, algorithm=JWTOps.key_management_alg, encryption=JWTOps.encryption_alg)
		encrypted = codecs.decode(encrypted)

		return encrypted



	@staticmethod
	def validate_json_token(token: str):
		token = jwe.decrypt(token, JWTOps.key)
		data = jwt.decode(token, JWTOps.key, algorithms=[JWTOps.jwt_encode_alg])

		return data


class UserAuth:
	pswd_context = settings.PSWD_CONTEXT
	sub_roles    = settings.SUBROLES

	@staticmethod
	def validate_username_and_password(username, password, db_session: Session):
		users = crud_ops.get_admins_by_username(usernames=[username], db_session=db_session)


		if not users:
			raise Exception('No such user')

		user = users[0]
		if not UserAuth.pswd_context.verify(password, user.hashed_pass):
			raise Exception('Invalid password')

		user_info = schema.AdminInfoSchema.from_orm(users[0])
		return user_info

	@staticmethod
	def get_password_hash(password):
		return UserAuth.pswd_context.hash(password)

	@staticmethod
	def can_access_dashboard(username: str, role: str, db_session: Session):
		try:
			user_in_database = crud_ops.get_admins_by_username(usernames=[username], db_session=db_session).pop()
		except IndexError:
			return False

		if not role == user_in_database.role:
			return False

		return True

	@staticmethod
	def can_manage(role: str):
		try:
			sub_roles = UserAuth.sub_roles[role].value
		except Exception as err:
			return []

		if not sub_roles:
			return []

		return sub_roles

	#@staticmethod
	#def update_not_allowed(user_to_update: str, admin_info: schema.AdminInfoSchema):
		##2. ADMIN WITH USERNAME == username CAN MANAGE ALL ADMINS IF root OR admin, ELSE ONLY username
		#if not any([
					#admin_info.username == user_to_update,
					#UserAuth.can_manage(admin_info.role)
					#]):

			#result = schema.ActionResult(operation = f'Update Admin: {user_to_update}',
										#msg       = f'You are not allowed to update: {user_to_update}',
										#next_url  = '/admin/all',)
			#encrypted_result = JWTOps.generate_json_token(result.dict())
			#return encrypted_result
		#return 0
