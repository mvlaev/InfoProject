import tempfile

from datetime import date
from pathlib import Path
from sqlalchemy.orm import Session

from .info_data_manager import data_prepare
from ..database import models, crud_ops
from ..schema   import data_schema as schema

########## UPLOAD MANAGEMENT
def upload_data(upload, db_session: Session):
	#1. PREPARE DATA
	with tempfile.TemporaryDirectory(dir='.') as d:
		file_path = Path(d, 'file.xlsx').resolve()

		with open(file_path, 'wb') as f:
			f.write(upload.file.read())

	#raise Exception(len(upload.file.read()))


		try:
			json_data = data_prepare.DataPrepare(file_path).json_all
		except Exception as err:
			#TODO: LOG ERR?
			return 'Cannot prepare data, check your file integrity.'

	#2. LOAD DATA IN DATABASE
	try:
		msg = move_to_database(json_upload_data=json_data, db_session=db_session)
	except Exception as err:
		#TODO: LOG ERR?
		return 'Cannot move data to database, check your file integrity.'

	return f'File {getattr(upload, "filename","")} uploaded successfully.'

#########TOP LEVEL UPLOAD DATA WRAPPER UTILITY FUNCTION
def move_to_database(json_upload_data: dict, db_session: Session):
	upload_schema = schema.UploadSchema(**json_upload_data)
	return save_data(upload_schema, db_session)

#########CRUD OPS WRAPPERS
#
def save_data(data: schema.UploadSchema, db_session: Session):
	'''Function save_data.

	1. Function that orchestrates the process of importing the newly received data.
	2. The function uses clients_manager, data_manager and crud_ops to accomplish
	the actual persistence of the data.
	3. Creates a message to be sent as a response to the upload request.
	'''

	msg = {'update':[],
		   'create':[],
		   'delete':[]
		   }

	if data.clients:
		msg = clients_manager(incoming_clients=data.clients, db_session=db_session, msg=msg)

	if data.readings:
		msg = data_manager(what='readings', incoming=data.readings, db_session=db_session, msg=msg)


	if data.electro:
		msg = crud_ops.create_something(what='electro', something=data.electro, db_session=db_session, msg=msg)

	#if data.water:
		#to_return['message'].append({'water': f'{recieved} water'})

	if data.warmup:
		msg = crud_ops.create_something(what='warmup', something=data.warmup, db_session=db_session, msg=msg)
	#if data.trash:
		#to_return['message'].append({'trash': f'{recieved} trash'})

	if data.common_warmup:
		msg = data_manager(what='common_warmup', incoming=data.common_warmup, db_session=db_session, msg=msg)
	return msg

def clients_manager(incoming_clients: list[schema.ClientProfileSchema], db_session: Session, msg: dict):
	'''Clients Manager.

	Manages the import of the newly received clients data.
	1. All the clients in DB but not in the new data are set to be deleted.
		- this triggers the internal deletion of all the records related to these clients in the db.
	2. All the records in the new data but not in DB are set to be created.
	3. All the records in both the new data and the DB are set to be updated.
		- the data in the newly received clients is used for the update
	4. The actions are performed in CUD order.
	'''

	all_contracts_in_db = set(client.contract_id for client in crud_ops.get_all_things(what='clients', db_session=db_session))
	incoming_contracts  = set(client.contract_id for client in incoming_clients)

	contracts_to_create_in_db   = incoming_contracts.difference(all_contracts_in_db)
	contracts_to_update_in_db   = incoming_contracts.intersection(all_contracts_in_db)
	contracts_to_delete_from_db = all_contracts_in_db.difference(incoming_contracts)

	clients_to_create_in_db   = [client for client in incoming_clients if client.contract_id in contracts_to_create_in_db]
	clients_to_update_in_db   = crud_ops.get_clients(contract_ids=contracts_to_update_in_db, db_session=db_session)
	clients_to_delete_from_db = crud_ops.get_clients(contract_ids=contracts_to_delete_from_db, db_session=db_session)


	msg = crud_ops.create_something(what='clients', something=clients_to_create_in_db, db_session=db_session, msg=msg)

	update_tuples = [(db_client, update_info) for db_client in clients_to_update_in_db
											  for update_info in incoming_clients
											  if db_client.contract_id == update_info.contract_id]

	msg = crud_ops.update_something(things=update_tuples, db_session=db_session, msg=msg)
	msg = crud_ops.delete_something(things=clients_to_delete_from_db, db_session=db_session, msg=msg)

	return msg

def data_manager(what: str, incoming: list, db_session: Session, msg: dict):
	'''Data Manager.

	Manages the import of the newly received general data that IS NOT SET FOR CASCADE DELETION IN THE DATABASE.
	This manager ensures that the old entries of the type "what" in the DB are cleared properly BEFORE the new ones are added.
	1. Deletes the entries of the current date if any.
	2. Deletes the entries older than months_of_backup.
	3. Creates the new records.
	'''

	#TODO: put that in settings!!!!!
	months_of_backup = 6
	##########

	today = date.today()
	before_date = today.year * 12 + today.month - months_of_backup
	before_date = date.fromisoformat(f'{before_date // 12}-{(before_date % 12) +1 :02d}-01')

	msg = crud_ops.delete_something_on_date(what=what, date=today, db_session=db_session, msg=msg)
	msg = crud_ops.delete_old_something(what=what, before_date=before_date, db_session=db_session, msg=msg)
	msg = crud_ops.create_something(what=what, something=incoming, db_session=db_session, msg=msg)

	return msg

#def common_warmup_manager(incoming_common_warmup: list[schema.CommonWarmupSchema], db_session: Session, msg: dict):
	#today = date.today()

	#msg = crud_ops.delete_something_on_date(what='common_warmup', date=today, db_session=db_session, msg=msg)
