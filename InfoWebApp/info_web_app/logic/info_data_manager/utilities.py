import time
monotonic = time.monotonic

class Stopwatch:
	def __init__(self):
		self.__start = None

	@property
	def running(self):
		if self.__start:
			return True
		return False

	@property
	def start(self):
		if self.__start:
			raise Exception('Stopwatch already running!')

		self.__start = monotonic()
		return self.__start

	@property
	def stop(self):
		if not self.__start:
			raise Exception('Stopwatch stopped befor started!')

		elapsed = monotonic() - self.__start
		self.__start = None
		return elapsed

