'''This module holds the class responsible for reading the data
from the .xlsx file.
'''

from datetime import date
from secrets import token_urlsafe
from logging import getLogger

import openpyxl

from .utilities import Stopwatch
from . import settings

class DataPrepare:
	'''Class DataPrepare.

	1. Reads the provided .xlsx file using openpyxl.
	2. Extracts the needed data from the corresponding sheets.
		- Currently the sheetnames are hard coded!
	3. Constructs the data structures required by the info_web_app API.
		- The structures are defined in info_web_app.schema.data_schema.UploadSchema
		pydantic model.
	'''

	def __init__(self, data_filename):
		self.logger = getLogger(__name__)

		self.data_filename = data_filename
		self.today = str(date.today())

		stopwatch = Stopwatch()

### SETUP THE WORK SHEETS FOR EVERY TYPE
		stopwatch.start

		try:
			wb = openpyxl.load_workbook(filename=self.data_filename, read_only=True, data_only=True)
		except openpyxl.utils.exceptions.InvalidFileException as err:
			self.logger.critical(err)
			raise

		try:
			self.clients_ws = wb['Info-dog']
			self.electro_ws = wb['Info-el']
			self.whater_ws  = None
			self.warmup_ws  = wb['Info-ot']
			self.trash_ws   = None

			self.common_warmup_ws = wb['Info-sgr']
		except KeyError as err:
			self.logger.critical(err)
			raise

		self.logger.info(f'files read in {stopwatch.stop}')
###############

### CONSTRUCT ALL READINGS
		stopwatch.start

		self.clients  = self.make_clients_list_of_dicts()
		self.readings = self.make_readings_list_of_dicts()

		self.electro = self.make_electro_list_of_dicts()
		self.water   = None
		self.warmup  = self.make_warmup_list_of_dicts()
		self.trash   = None

		self.common_warmup = self.make_common_warmup_list_of_dicts()

		self.json_all = {
			'clients' : self.clients,
		    'readings': self.readings,
		    'electro' : self.electro,
		    'warmup'  : self.warmup,
		    'common_warmup': self.common_warmup,
		    #TODO:
		    #ADD OTHER SHIT HERE
		  }

		self.logger.info(f'data generated in {stopwatch.stop}')
#################

		wb.close()
		self.logger.info(f'workbook {wb} closed')

	def make_electro_list_of_dicts(self):
		electro = []
		#FOR ALL ROWS
		last_meter_name = ''
		i = 1

		for value in self.electro_ws.values:
			el = {}
			#IF THERE IS A contract_id (not null or None):
			if type(value[8]).__name__ == 'int' and value[8] > 0:
				current_meter_name = value[0]
				participation_coef = value[4]

				#if current_meter_name == last_meter_name:
				if participation_coef < 1:
					el['meter_name'] = current_meter_name + f'--(k{i})'
					i += 1
				else:
					el['meter_name'] = current_meter_name
					i = 1

				el['reading_old'] = value[1] if value[1] else 0.
				el['reading_new'] = value[2] if value[2] else 0.
				el['reading_diff']        = value[2] - value[1] if value[1] and value[2] else 0.
				el['transformation_coef'] = value[3]
				el['participation_coef']  = value[4]
				el['consumption']         = value[5]
				el['price_single']        = value[6] if value[6] else 0.
				el['price_end']           = value[7]
				el['contract_id']         = value[8]
				el['date']                = self.today

				electro.append(el)
				#last_meter_name = current_meter_name

		return electro

	def make_warmup_list_of_dicts(self):
		warmup = []

		for value in self.warmup_ws.values:
			if type(value[9]).__name__ == 'int' and value[9] > 0:
				reading = {}

				reading['contract_id']             = value[9]
				reading['date']                    = self.today
				reading['unit']                    = value[1]
				reading['building']                = value[0]
				reading['volume']                  = value[2]
				reading['coef']                    = value[3]
				reading['reduced_volume']          = value[4]
				reading['power']                   = value[5]
				reading['energy']                  = value[6]
				reading['energy_plus_maintenance'] = value[7]
				reading['price_end']               = value[8]

				warmup.append(reading)
		return warmup

	def make_water_list_of_dicts(self): pass
	def make_hot_water_list_of_dicts(self): pass
	def make_trash_list_of_dicts(self): pass

	def make_clients_list_of_dicts(self):
		clients = []

		for value in self.clients_ws.values:
			if type(value[0]).__name__ == 'int' and value[0] > 0:
				client = {}
				client['contract_id'] = value[0]
				client['full_name']   = value[1]
				client['email']       = value[2] if value[2] else f'not-an-email{value[0]}@no-mail.none'
				client['wants_report']= True #MUST BE False if value[10] == 'no' else True

				clients.append(client)
		return clients

	def make_readings_list_of_dicts(self):
		readings = []

		for value in self.clients_ws.values:
			if type(value[0]).__name__ == 'int' and value[0] > 0:
				reading = {}
				reading['contract_id'] = value[0]
				reading['date']        = self.today
				reading['url_suffix']  = token_urlsafe() #f'{value[0]}' #must be token_urlsafe()
				readings.append(reading)
		return readings

	def make_common_warmup_list_of_dicts(self):
		common_warmup = []

		for value in self.common_warmup_ws.values:
			if value[0] in settings.buildings and not value[27] == None:
				reading = {}

				reading['date']                  = self.today
				reading['building']              = value[0]
				reading['total_energy']          = value[27]
				reading['total_reduced_volume']  = value[28]# if value[28] else None
				reading['energy_reduced_volume'] = value[29]# if value[29] else None
				reading['total_power']           = value[30]# if value[30] else None
				reading['energy_per_power']      = value[31]# if value[31] else None
				reading['single_price']          = value[32]
				reading['total_price']           = value[33]
				reading['maintenance_percent']   = value[34]

				common_warmup.append(reading)

		return common_warmup

	def make_common_water_list_of_dicts(self): pass
	def make_common_hot_water_list_of_dicts(self): pass
	def make_common_trash_list_of_dicts(self): pass
