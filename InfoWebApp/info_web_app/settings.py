'''
This module provides configuration constants for the project.
'''

###DATABASE
#Database related constants

###SQLite database:
#In memory
#SQLALCHEMY_DATABASE_URL = "sqlite:///:memory:"

#In a file
from pathlib import Path

current_abs = Path(__file__).resolve().parent
SQLALCHEMY_DATABASE_URL = f"sqlite:///{current_abs}/../info_database.db"

