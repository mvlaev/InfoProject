from fastapi import Depends, Form
from jose import exceptions
from pydantic import ValidationError

from .database_session import get_session
from ..auth import auth_ops
from ..schema import data_schema as schema

class BaseTokenCheck:
	operation   = 'Token Check'
	success_msg = 'Token is OK'
	login_url   = '/admin/'

	def __init__(self, csrf_token: str = Form(None)):
		self._token = csrf_token
		self._msg = None

		self._personal_jwt_container = self._validate_jwt()
		self._grant_permission = self._check()

		#self._result = None

	@property
	def token_is_valid(self):
		if self._personal_jwt_container.error:
			return False
		return True

	@property
	def permission_granted(self):
		return self._grant_permission

	@property
	def personal_jwt_container(self):
		return self._personal_jwt_container

	@property
	def message(self):
		return self._msg

	@property
	def result(self):
		if not self.permission_granted:
			return schema.ActionResult(operation = self.operation,
										 msg       = self.personal_jwt_container.error,
										 next_url  = self.login_url,).dict()
		return None

	@property
	def encrypted_result(self):
		encrypted_result = auth_ops.JWTOps.generate_json_token(self.result)
		return encrypted_result

	def _validate_jwt(self):
		try:
			data = auth_ops.JWTOps.validate_json_token(self._token)
		except (exceptions.JWTError, exceptions.JWEError, TypeError) as err:
			#TODO: MAYBE LOG THIS KIND OF STUFF?
			self._msg = str(err)
			return schema.TokenContainer(error=str(err))

		try:
			admin_info = schema.AdminInfoSchema(**data)
		except ValidationError as err:
			self._msg = str(err)
			return schema.TokenContainer(error=str(err))

		self._msg = self.success_msg
		return schema.TokenContainer(token=self._token, admin_info=admin_info)

	def _check(self):
		if self.token_is_valid:
			return True
		return False


class PermissionToLogin(BaseTokenCheck):
	operation    = 'Login'
	op_denied_url = '/admin/'
	success_url = '/admin/dashboard/'
	success_msg = 'Successful Login.'

	def __init__(self,
			  username: str  = Form(None),
			  password: str  = Form(None),
			  csrf_token: str = Form(None),
			  db_session     = Depends(get_session)):
		super().__init__(csrf_token)
		self.__checks_passed = self.__check(username, password, db_session)

	@property
	def permission_granted(self):
		return self.__checks_passed

	@property
	def result(self):
		if self.permission_granted:
			return schema.ActionResult(operation = self.operation,
										 msg       = self.message,
										 next_url  = self.success_url,).dict()
		else:
			return schema.ActionResult(operation = self.operation,
										 msg       = self.message,
										 next_url  = self.op_denied_url,).dict()


	def __check(self, username, password, db_session):
		if not self.token_is_valid:
			self._msg = self.personal_jwt_container.error
			return False

		try:
			user_info = auth_ops.UserAuth.validate_username_and_password(username=username, password=password, db_session=db_session)
		except Exception as err:
			self._personal_jwt_container = schema.TokenContainer(error=str(err))
			self._msg = str(err)
			return False
		else:
			data = schema.AdminInfoSchema.from_orm(user_info).dict()
			self._personal_jwt_container = schema.TokenContainer(token=auth_ops.JWTOps.generate_json_token(data=data), admin_info=user_info)
			self._msg = self.success_msg
			return True

class PermissionDashboard(BaseTokenCheck):
	def __init__(self, csrf_token: str = Form(None), db_session = Depends(get_session)):
		self.db_session = db_session
		super().__init__(csrf_token)

		#self.__checks_passed = self.__check()

	#@property
	#def permission_granted(self):
		#return self.__checks_passed

	@property
	def result(self):
		if not self.permission_granted:
			return schema.ActionResult(operation = self.operation,
										 msg       = self.message,
										 next_url  = self.login_url,).dict()
		return None


	def _check(self):
		if not self.token_is_valid:
			self._msg = self.personal_jwt_container.error
			return False

		if not auth_ops.UserAuth.can_access_dashboard(username=self.personal_jwt_container.admin_info.username, role=self.personal_jwt_container.admin_info.role, db_session=self.db_session):
			self._msg = 'User not recognized'
			return False

		self._msg = 'OK'
		return True

class PermissionToUpdate(BaseTokenCheck):
	operation = 'Update User'
	login_url = '/admin/'
	op_denied_url = '/admin/dashboard'

	def __init__(self, username: str, csrf_token: str = Form(None),):
		self._username = username
		super().__init__(csrf_token)

		#self.__checks_passed = self.__check()

	#@property
	#def permission_granted(self):
		#return self.__checks_passed

	@property
	def result(self):

		if not self.token_is_valid:
			return schema.ActionResult(operation = self.operation,
 										 msg       = self.message,
 										 next_url  = self.login_url,).dict()
		elif not self.permission_granted:
			return schema.ActionResult(operation = self.operation,
										 msg       = self.message,
										 next_url  = self.op_denied_url,).dict()
		#else:
			#result = schema.ActionResult(operation = self.operation,
										 #msg       = self.message,
										 #next_url  = self.redirect_url,)
		return None

	def _check(self):
		#1. Check Token
		if not self.token_is_valid:
			self._msg = self.personal_jwt_container.error
			return False

		#2. ADMIN WITH USERNAME == username CAN MANAGE ALL ADMINS IF root OR admin, ELSE ONLY username
		if not any([
				self.personal_jwt_container.admin_info.username == self._username,
				auth_ops.UserAuth.can_manage(self.personal_jwt_container.admin_info.role)
				]):
			self._msg = f'You are not allowed to update: {self._username}'
			return False

		self._msg = 'OK'
		return True

class PermissionToCreateAdmin(BaseTokenCheck):
	operation = 'Create Admin'
	login_url = '/admin/'
	op_denied_url = '/admin/dashboard'
	op_denied_msg = 'You are not allowed to create admins.'


	def _check(self):
		#1. Check Token
		if not self.token_is_valid:
			self._msg = self.personal_jwt_container.error
			return False

		#2. CHECK IF ADMIN CAN MANAGE SUB ROLES
		if not auth_ops.UserAuth.can_manage(self.personal_jwt_container.admin_info.role):
			self._msg = self.op_denied_msg
			return False

		return True

	@property
	def result(self):
		if not self.token_is_valid:
			return  schema.ActionResult(operation = self.operation,
										 msg       = self.personal_jwt_container.error,
										 next_url  = self.login_url,).dict()

		elif not self.permission_granted:
			return schema.ActionResult(operation = self.operation,
										 msg       = self.message,
										 next_url  = self.op_denied_url,).dict()

		return None


class PermissionToCreateRole(PermissionToCreateAdmin):
	operation       = 'Create Admin'
	login_url       = '/admin/'
	op_denied_url   = '/admin/dashboard/'
	role_denied_msg = 'You are not allowed to create admins with role:'

	def __init__(self, role: str = Form(None), csrf_token: str = Form(None)):
		self._role = role
		super().__init__(csrf_token)

	def _check(self):
		#1. CHECK IF ADMIN CAN CREATE ADMINS
		if not super()._check():
			return False

		#2. CHECK IF ADMIN CAN CREATE GIVEN ROLE
		if not self._role in auth_ops.UserAuth.can_manage(self.personal_jwt_container.admin_info.role):
			self._msg = f'{self.role_denied_msg} {self._role}.'
			return False

		return True

class PermissionToDeleteAdmin(PermissionToCreateRole):
	operation = 'Delete Admin'
	op_denied_msg = 'You are not allowed to delete admins.'
	role_denied_msg = 'You are not allowed to delete admins with role:'

class PermissionToListAdmins(PermissionToCreateAdmin):
	operation = 'List Admins'
	op_denied_msg = 'You are not allowed to list admins.'

class PermissionToUploadData(PermissionToCreateAdmin):
	operation = 'Upload Data'
	op_denied_msg = 'You are not allowed to upload data.'
