from ..database.config import LocalSession

def get_session():
	session = LocalSession()
	try:
		yield session
	finally:
		session.close()
