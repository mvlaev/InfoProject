from fastapi  import Depends, Form, File, UploadFile
from jose     import exceptions
from pydantic import ValidationError

from .                 import auth_classes
from .database_session import get_session
from ..auth            import auth_ops
from ..database        import crud_ops
from ..logic           import upload_data_logic
from ..schema          import data_schema as schema

class DepBase:
	#operation = None
	go_to_url = None
	op_fail_url = None
	success_url = None
	fail_msg = None
	success_msg = 'OK'

	def __init__(self, permission, db_session):
		self._permission = permission
		self._db_session = db_session

		if not permission.permission_granted:
			self._msg = self._permission.message
			return

		self._msg = self._logic()

	@property
	def message(self):
		return self._msg

	@property
	def permission(self):
		return self._permission

	@property
	def db_session(self):
		return self._db_session

	@property
	def encrypted_result(self):
		return auth_ops.JWTOps.generate_json_token(self.result.dict())

	@property
	def result(self):
		if not self.permission.token_is_valid:
			return schema.ActionResult(operation = self.operation,
									   msg       = self.permission.personal_jwt_container.error,
									   next_url  = self.permission.login_url,)

		if not self.permission.permission_granted:
			return schema.ActionResult(operation = self.operation,
									   msg       = self.permission.message,
									   next_url  = self.permission.op_denied_url,)

		return schema.ActionResult(operation = self.operation,
								   msg       = self.message,
								   next_url  = self.go_to_url,)

	def _logic(self): pass

class UploadManagement(DepBase):
	operation = 'Upload Data'
	go_to_url = '/admin/dashboard/'
	fail_msg = 'Upload Failed.'

	def __init__(self,
				 upload: UploadFile = File(None),
				 permission = Depends(auth_classes.PermissionToUploadData),
				 db_session = Depends(get_session)):
		self.__upload = upload
		super().__init__(permission=permission, db_session=db_session)

	def _logic(self):
		return upload_data_logic.upload_data(upload=self.__upload, db_session=self.db_session)


class CreateAdmin(DepBase):
	operation = 'Create Admin'
	go_to_url = '/admin/dashboard/'

	def __init__(self,
				permission          = Depends(auth_classes.PermissionToCreateRole),
				username: str       = Form(None),
				role: str           = Form(None),
				password: str       = Form(None),
				password_again: str = Form(None),
				email: str          = Form(None),
				db_session          = Depends(get_session)):

		self.__username = username
		self.__role = role
		self.__password = password
		self.__password_again = password_again
		self.__email = email

		super().__init__(permission=permission, db_session=db_session)

	def _logic(self):
		if not self.permission.permission_granted:
			return self.permission.message

		if not self.__password == self.__password_again:
			return 'Passwords do not match.'

		# TODO: email check??

		user_schema = schema.AdminSchema(username=self.__username,
										 role=self.__role,
										 hashed_pass=auth_ops.UserAuth.get_password_hash(self.__password),
										 email=self.__email,
										)

		try:
			crud_ops.create_something(what='admins', something=[user_schema], db_session=self.db_session, msg={'create':[]})
		except Exception as err:
			return f'database error: {str(err).split()[0]}'
		else:
			return 'Success!'


class ListAdmins(DepBase):
	operation = 'List Admins'
	go_to_url = '/admin/dashboard/'

	def __init__(self,
				 permission = Depends(auth_classes.PermissionToListAdmins),
				 db_session = Depends(get_session)):
		self.admins = None
		super().__init__(permission=permission, db_session=db_session)

	def _logic(self):
		if not self.permission.permission_granted:
			return self.permission.message

		self.admins = crud_ops.get_all_things(what='admins', db_session=self.db_session)
		return 'OK'


class UpdateAdmin(DepBase):
	operation = 'Update Admin'
	go_to_url = '/admin/dashboard/'

	def __init__(self,
				 username: str,
				 role: str               = Form(None),
				 email: str              = Form(None),
				 new_password: str       = Form(None),
				 new_password_again: str = Form(None),
				 permission              = Depends(auth_classes.PermissionToUpdate),
				 db_session              = Depends(get_session),
				 ):

		self.username           = username
		self.role               = role
		self.email              = email
		self.new_password       = new_password
		self.new_password_again = new_password_again

		super().__init__(permission=permission, db_session=db_session)

	def _logic(self):
		if not self.permission.permission_granted:
			return self.permission.message

		try:
			admin_in_database = crud_ops.get_admins_by_username(usernames=[self.username], db_session=self.db_session).pop()
		except IndexError:
			return f'Admin with username: {self.username} NOT FOUND.'

		if self.email:
			#1. TODO: CHECK MAIL
			#2. UPDATE
			try:
				admin_in_database.email = self.email
				self.db_session.commit()
			except Exception as err:
				return f'Database error: {str(err).split()[0]}'

			return 'Admin updated successfully.'

		if self.role:
			if self.role in auth_ops.UserAuth.can_manage(self.permission.personal_jwt_container.admin_info.role):
				# UPDATE ROLE
				try:
					admin_in_database.role = self.role
					self.db_session.commit()
				except Exception as err:
					return f'Database error: {str(err).split()[0]}'

				return 'Admin updated successfully.'
			#CAN NOT UPDATE ROLE
			return 'Cannot update role.'

		if self.new_password:
			if self.new_password == self.new_password_again:
				# UPDATE PASS
				try:
					admin_in_database.hashed_pass = auth_ops.UserAuth.get_password_hash(self.new_password)
					self.db_session.commit()
				except Exception as err:
					return f'Database error: {str(err).split()[0]}'

				return 'Admin updated successfully.'
			return 'Passwords do not match.'

		return 'Nothing to update?'


class DeleteAdmin(DepBase):
	operation = 'Delete Admin'
	go_to_url = '/admin/all/'

	def __init__(self,
				 username: str,
				 permission = Depends(auth_classes.PermissionToDeleteAdmin),
				 db_session = Depends(get_session)):
		self.username = username
		super().__init__(permission=permission, db_session=db_session)

	def _logic(self):
		if not self.permission.permission_granted:
			return self.permission.message

		try:
			admin_in_database = crud_ops.get_admins_by_username(usernames=[self.username], db_session=self.db_session)[0]
		except IndexError:
			return f'Admin with username: {self.username} NOT FOUND.'

		try:
			crud_ops.delete_something(things=[admin_in_database], db_session=self.db_session, msg={'delete':[]})
		except Exception as err:
			return f'Database error: {str(err).split()[0]}'

		return f'Admin {self.username} deleted.'

class GetClients(DepBase):
	operation = 'Get Report'
	go_to_url = '/admin/dashboard/'

	def __init__(self,
				 permission = Depends(auth_classes.PermissionDashboard),
				 db_session = Depends(get_session)):
		self.clients = None
		super().__init__(permission=permission, db_session=db_session)

	def _logic(self):
		if not self.permission.permission_granted:
			return self.permission.message

		self.clients = crud_ops.get_all_things(what='clients', db_session=self.db_session)
		return 'OK'

class GetClientReports(DepBase):
	operation = 'Get Client Reports'
	go_to_url = '/admin/dashboard/'

	def __init__(self,
				 contract_id = Form(None),
				 permission = Depends(auth_classes.PermissionDashboard),
				 db_session = Depends(get_session)):
		self.contract_id = contract_id
		self.readings = None
		super().__init__(permission=permission, db_session=db_session)

	def _logic(self):
		if not self.permission.permission_granted:
			return self.permission.message

		self.readings = crud_ops.get_client_readings_by_contract_id(contract_id=self.contract_id, db_session=self.db_session)
		return 'OK'
