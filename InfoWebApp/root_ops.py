import getpass

#from passlib import hash
from info_web_app.auth.settings import PSWD_CONTEXT

from info_web_app.database.config import LocalSession
from info_web_app.database import models
from info_web_app.schema import data_schema as schema

#TODO: PUT THAT IN SETTINGS
roles = ["root", "admin", "op"]

def create_user():
	username = str(input('username: '))
	password = getpass.getpass('password: ')
	email = str(input('email: '))
	role = str(input(f'role {str(roles)}: '))

	hashed_pass = PSWD_CONTEXT.hash(password)
	admin_schema = schema.AdminSchema(username=username, hashed_pass=hashed_pass, role=role, email=email)
	admin = models.Admin(**admin_schema.dict())

	with LocalSession() as s:
		try:
			s.add(admin)
			s.commit()
		except Exception as err:
			print('error!')
			print(err)

if __name__ == '__main__':
	match input('''
1. Create operator user in database (C, 1)
'''):
		case '1' | 'c' |'C':
			print('ok')
			create_user()
		case _:
			print('bad')
